import React from "react";
import Header from './components/Header';
import {
  Route,
  Routes,
} from "react-router-dom"
import HomePage from './pages/homePage/homePage';
import FavouritesPage from './pages/favouritesPage/favouritesPage';
import CartPage from "./pages/cartPage/cartPage";

function App(){
    return(
      <>
      <Header/>
        <Routes>
          <Route path="/" element={<HomePage/>}/> 
          <Route path="/favourites" element={<FavouritesPage/>}/>
          <Route path="/cart" element={<CartPage/>}/>
        </Routes>
      </>
   )
}


export default App;
