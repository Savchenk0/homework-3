import React, { useState } from "react"
import PropTypes from 'prop-types';
function Star(props){
        const [isFav,setIsFav] = useState(false)
        const {lsKey,handleClick,manageClick} = props
        if (localStorage.getItem(lsKey) === "true") {
        return (
            <img onClick={()=>{
                handleClick()
                setIsFav(!isFav)
                manageClick()
            }} src="/img/star-True.svg"/>
        )
        }
        return(
            <img onClick={()=>{
                handleClick()
                setIsFav(!isFav)
                manageClick()
            }} src="/img/star.svg"/>
        )
    }
Star.propTypes = {
  lsKey:PropTypes.number,
  handleClick:PropTypes.func,
}
export default Star