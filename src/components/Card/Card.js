import styles from "./Card.module.scss"
import React, { useState } from "react";
import {useLocation} from 'react-router-dom'
import Star from "../Star";
import PropTypes from 'prop-types';

function Card(props) {
        const [showCard,setShowCard] = useState(true)
        const {name,artikul,color,url,price,handleClick,cartText} = props
        const {pathname} = useLocation()
       if((!showCard && pathname === '/favourites')||(!showCard && pathname === '/cart')){
           return null
       }
        return(
            <div className={styles.cardSlot} key={artikul}>
                <img className={styles.cardSlotImage} src={url}/>
                <div className={styles.cardSlotContent}>
             <div className={styles.cardSlotContentNameWrapper}><p className={styles.cardSlotContentName}>{name}</p><Star manageClick={()=>setShowCard(!showCard)} lsKey={artikul} handleClick={()=>{
                const tempStatus = localStorage.getItem(artikul)
                localStorage.removeItem(artikul)
                if (tempStatus === "false"){
                    localStorage.setItem(artikul,true)
                }
                else{
                    localStorage.setItem(artikul,false)
                }
                }}/></div>
                <p className={styles.cardSlotContentText}>Lorem ipsum dolor sit amet, con adipiscing elit, sed diam nonu.</p>
                <div className={styles.cardSlotContentFooter}><span className={styles.cardSlotContentFooterPrice}>${price}</span> <a onClick={e=>{e.preventDefault() ; handleClick();setShowCard(!showCard)}}href="#" className={styles.cardSlotContentFooterBtn}>{cartText}</a></div>
                </div>
            </div>
        )

           
}
Card.propTypes = {
    name:PropTypes.string,
    handleClick:PropTypes.func,
    artikul:PropTypes.number,
    color:PropTypes.string,
    url:PropTypes.string,
    price:PropTypes.oneOfType(PropTypes.string,PropTypes.number),
}
Card.defaultProps={
    color:'blue',
    url:"/img/ifNoUrl.jpg",
}

export default Card