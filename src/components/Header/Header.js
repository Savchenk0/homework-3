import React from "react";
import PropTypes from 'prop-types';
import styles from './Header.module.scss';
import { Link } from "react-router-dom";
export default function Header(){
        return (
            <div className={styles.HeaderWrapper}>
                <ul className={styles.HeaderList}>
                    <li ><Link className={styles.HeaderListItem} to="/">Home</Link></li>
                    <li><Link className={styles.HeaderListItem} to="/cart">Cart</Link></li>
                    <li><Link className={styles.HeaderListItem} to="/favourites">Favourites</Link></li>
                </ul>
            </div>
        )
}