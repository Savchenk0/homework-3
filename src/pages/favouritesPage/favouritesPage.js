import React,{ useEffect, useState } from "react"
import styles from './favouritesPage.module.scss';
import Modal from '../../components/Modal';
import Card from '../../components/Card';
import btnStyles from '../../components/Modal/Modal.module.scss'
import axios from 'axios';

export default function FavouritesPage(){
    const [isModal1,setIsModal1] = useState(false)
    const [productList,setProductList] = useState([])
    const [currArtikul,setCurrArtikul] = useState('')

    useEffect(()=>{      
      try{
      axios('/productList.json')
       .then(res =>{
         const {data} = res;
        const favouritesArray = [];
        for (let elem of data){
          const {artikul} = elem
          if (!localStorage.getItem(artikul)){
            localStorage.setItem(artikul,false )
          }
        }
        
        res.data.forEach((elem)=> {if (localStorage.getItem(elem.artikul) ==="true"){favouritesArray.push(elem)}})
       setProductList(favouritesArray)
      }
      )
    }
      catch(e){
        console.log(e)
      }  
    },[]
    )
    return(         
  <div className={styles.root}>
  <Modal handleClick={()=> setIsModal1(false)} header="Do you want to add this to cart?" isVisible={isModal1} closeButton={true} text="go for it?" actions={()=>{return <div><a onClick={()=>{
  if (localStorage.getItem(`${currArtikul}Cart`) !== 'true'){
    localStorage.setItem(`${currArtikul}Cart`,'true')
  }
  setIsModal1(false)}}className={btnStyles.modalContentBodyBtn}>Ok</a>
                  <a onClick={()=>{
                    setIsModal1(false)}} className={btnStyles.modalContentBodyBtn}>Cancel</a></div>}}/>
 <div className={styles.cardHolder}>

 {productList.map(({name,artikul,url,price}) =><Card handleClick={()=>{
     setIsModal1(true)
     setCurrArtikul(artikul)
 }}name={name} artikul={artikul} cartText='ADD TO  CART' key={artikul} url={url} price={price}/> )}
 </div>
 </div> 
    )
}











